<?php

/**
 * @file
 * Admin settings page for Private Node Types module.
 */

/**
 * Form builder callback for admin settings form of Private Node Types module.
 */
function private_node_types_settings_form() {
  $form = array();
  $opts = node_type_get_names();
  $form['private_node_types_types'] = array(
    '#type' => 'select',
    '#title' => t('Node types'),
    '#description' => t('Select node types to use as admin-only nodes'),
    '#default_value' => variable_get('private_node_types_types', array()),
    '#options' => $opts,
    '#multiple' => TRUE,
    '#size' => min(count($opts), 12),
  );

  $opts = user_roles();
  $form['private_node_types_roles'] = array(
    '#type' => 'select',
    '#title' => t('User roles'),
    '#description' => t('Select user roles to consider admin roles'),
    '#default_value' => variable_get('private_node_types_roles', array()),
    '#multiple' => TRUE,
    '#required' => TRUE,
    '#options' => user_roles(),
    '#size' => min(count($opts), 10),
  );

  return system_settings_form($form);
}
