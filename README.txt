This is an example module for basic node access control without implementing 
any cache-blocking hooks. 

This module has only one simple task, and it can do it in the lightest way 
possible: Make certain node types are only accessible if the user has one 
or more of the selected roles.

Note that this module is not suitable for fine access control. This module 
is all about simplicity and performance (while not forgetting anything about 
security of course), but if you are looking for per-node access controls, 
per-operation access controls, and anything beyond that, I'm afraid this 
module is not for you.

To make use of the module, install the module as usual, and go to
Administer > Configuration > Content authoring > Private Node Types
(admin/config/content/private_node_types)

You can choose one or more node types as private node type and select
which roles can access those private nodes.
